package com.eam.decimalmatcher

fun matchDecimal(input: String): Double {
    return try {
        matchDecimalWithCommaAsDecimalPoint(input)
    } catch (e: Exception) {
        matchDecimalWithDotAsDecimalPoint(input)
    }
}

private fun matchDecimalWithCommaAsDecimalPoint(input: String): Double {
    return "[^\\d]*((\\d+(?:\\.\\d{3})*),(\\d*))[^\\d]*".toRegex().matchEntire(input)?.let {
        it.groups[2]?.value?.replace(".", "")
            ?.plus(".")
            ?.plus(it.groups[3]?.value ?: "00")
            ?.toDouble()
    } ?: throw IllegalArgumentException()
}

private fun matchDecimalWithDotAsDecimalPoint(input: String): Double {
    return "[^\\d]*((\\d+(?:,\\d{3})*)\\.(\\d*))[^\\d]*".toRegex().matchEntire(input)?.let {
        it.groups[2]?.value?.replace(",", "")
            ?.plus(".")
            ?.plus(it.groups[3]?.value ?: "00")
            ?.toDouble()
    } ?: throw IllegalArgumentException()
}