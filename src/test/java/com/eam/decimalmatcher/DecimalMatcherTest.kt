package com.eam.decimalmatcher

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class DecimalMatcherTest {

    @Test
    fun `when input contains decimal then it should be matched correctly`() {
        assertEquals(1_234.0, matchDecimal("€ 1234,00"))
        assertEquals(1_234.0, matchDecimal("€ 1234,00/u"))
        assertEquals(1_234.0, matchDecimal("€ 1234,00/uur"))
        assertEquals(1_234.0, matchDecimal("€ 1234,00/h"))
        assertEquals(1_234.0, matchDecimal("€ 1234,00/hour"))
        assertEquals(1_234.0, matchDecimal("€ 1.234,00"))
        assertEquals(1_234_567.0, matchDecimal("€ 1.234.567,00"))
        assertEquals(1_234.0, matchDecimal("€ 1.234,00/u"))
        assertEquals(1_234.0, matchDecimal("€ 1.234,00/uur"))
        assertEquals(1_234.0, matchDecimal("€ 1.234,00/h"))
        assertEquals(1_234.0, matchDecimal("€ 1.234,00/hour"))

        assertEquals(1_234.0, matchDecimal("1234,00 €"))
        assertEquals(1_234.0, matchDecimal("1234,00 € /u"))
        assertEquals(1_234.0, matchDecimal("1234,00 € /uur"))
        assertEquals(1_234.0, matchDecimal("1234,00 € /h"))
        assertEquals(1_234.0, matchDecimal("1234,00 € /hour"))
        assertEquals(1_234.0, matchDecimal("1.234,00 €"))
        assertEquals(1_234_567.0, matchDecimal("1.234.567,00"))
        assertEquals(1_234.0, matchDecimal("1.234,00 € /u"))
        assertEquals(1_234.0, matchDecimal("1.234,00 € /uur"))
        assertEquals(1_234.0, matchDecimal("1.234,00 € /h"))
        assertEquals(1_234.0, matchDecimal("1.234,00 € /hour"))

        assertEquals(1_234.0, matchDecimal("€ 1234.00"))
        assertEquals(1_234.0, matchDecimal("€ 1234.00/u"))
        assertEquals(1_234.0, matchDecimal("€ 1234.00/uur"))
        assertEquals(1_234.0, matchDecimal("€ 1234.00/h"))
        assertEquals(1_234.0, matchDecimal("€ 1234.00/hour"))
        assertEquals(1_234.0, matchDecimal("€ 1,234.00"))
        assertEquals(1_234_567.0, matchDecimal("€ 1,234,567.00"))
        assertEquals(1_234.0, matchDecimal("€ 1,234.00/u"))
        assertEquals(1_234.0, matchDecimal("€ 1,234.00/uur"))
        assertEquals(1_234.0, matchDecimal("€ 1,234.00/h"))
        assertEquals(1_234.0, matchDecimal("€ 1,234.00/hour"))

        assertEquals(1_234.0, matchDecimal("1234.00 €"))
        assertEquals(1_234.0, matchDecimal("1234.00 € /u"))
        assertEquals(1_234.0, matchDecimal("1234.00 € /uur"))
        assertEquals(1_234.0, matchDecimal("1234.00 € /h"))
        assertEquals(1_234.0, matchDecimal("1234.00 € /hour"))
        assertEquals(1_234.0, matchDecimal("1,234.00 €"))
        assertEquals(1_234_567.0, matchDecimal("1,234,567.00"))
        assertEquals(1_234.0, matchDecimal("1,234.00 € /u"))
        assertEquals(1_234.0, matchDecimal("1,234.00 € /uur"))
        assertEquals(1_234.0, matchDecimal("1,234.00 € /h"))
        assertEquals(1_234.0, matchDecimal("1,234.00 € /hour"))
    }

}